<?php
/**
 * Functions
 */

add_filter( 'show_admin_bar', 'remove_admin_bar' );
function remove_admin_bar(){
	return false;
}

add_action( 'after_setup_theme', 'custom_theme_setup' );
function custom_theme_setup(){
	add_theme_support('automatic-feed-links');
	add_theme_support('title-tag');
	add_theme_support('custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	));
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(1200, 9999);
	register_nav_menus(array(
		'primary' => __('Primary Menu', 'twentysixteen')
	));
}

add_action( 'wp_enqueue_scripts', 'add_theme_files' );
function add_theme_files(){
	wp_enqueue_style('style', get_stylesheet_uri()); 
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css', array(), '1.0', 'all'); 
	wp_enqueue_script('owl', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array ('jquery'), 1.0, true);
	wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js', array ('jquery'), 1.0, true);
}

include_once("inc/theme-options.php");
include_once("inc/load-template-part.php");