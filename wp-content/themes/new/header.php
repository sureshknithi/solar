<?php
/**
 * Header
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="site-header">
	<div class="branding">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 d-flex align-items-center">
					<button type="button" class="mobile-menu-handler toggle-menu btn btn-clear hidden-lg-up"><i class="fa fa-bars"></i></button>
					<div class="logo">
						<?php the_custom_logo(); ?>
					</div>
				</div>
				<div class="col-lg-6 d-flex justify-content-center align-items-lg-center">
					<div class="header-menu">
						<div class="header-menu-container">
							<div class="mobile-menu-header hidden-lg-up">
								<a href="javascript:void(0);" class="toggle-menu"><i class="fa fa-long-arrow-left"></i></a><span>Menu</span>
							</div>
							<?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'primary-menu',));?>
						</div>
					</div>
				</div>
				<div class="col-lg-3 d-flex justify-content-end align-items-center hidden-md-down">
					<div class="social-links">
						<ul>
							<?php if(get_theme_mod('facebook')): ?><li><a href="<?php echo get_theme_mod('facebook');?>"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
							<?php if(get_theme_mod('twitter')): ?><li><a href="<?php echo get_theme_mod('twitter');?>"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
							<?php if(get_theme_mod('linkedin')): ?><li><a href="<?php echo get_theme_mod('linkedin');?>"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
							<?php if(get_theme_mod('pinterest')): ?><li><a href="<?php echo get_theme_mod('pinterest');?>"><i class="fa fa-pinterest"></i></a></li><?php endif; ?>
							<?php if(get_theme_mod('youtube')): ?><li><a href="<?php echo get_theme_mod('youtube');?>"><i class="fa fa-youtube"></i></a></li><?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<main>
<?php if(is_front_page()): ?>
	<?php load_template_part('template-parts/components/slider-main', array('slider_id' => 'home-slider')); ?>
<?php else: ?>
	<?php load_template_part('template-parts/components/banner-main', array('banner_id' => 'header-banner')); ?>
<?php endif; ?>