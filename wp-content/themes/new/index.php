<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section class="page-content">
	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="col-sm-8">
					<div class="main-title">
						<span class="tagline-one"><?php echo get_field('tag_line'); ?></span>
						<h2 class="title-one"><?php the_title(); ?></h2>					
					</div>
					<div class="desc"><?php echo get_the_content();?></div>
				</div>
				<div class="col-sm-4">
					<?php the_post_thumbnail(); ?>
					<?php get_sidebar(); ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</section>



<?php get_footer();
