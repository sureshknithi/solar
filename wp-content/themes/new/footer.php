<?php
/**
 * Footer
 */
?>
</main>
<footer class="site-footer">
	<div class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="footer-block">
						<h3 class="footer-logo"><?php the_custom_logo(); ?></h3>
						<div class="block-desc">
							Sed cursus tortor neque, sit amet dignissim purus pretium vel. Nullam et sodales sem. Fusce elit urna, accumsan id mollis eu, egestas a nunc.
						</div>
						<div class="social-links">
							
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="footer-block">
						<h3 class="block-title title-three">Contact us</h3>
						<div class="block-desc">
							<div class="c-item">
								<span class="icon"><i class="fa fa-map-marker"></i></span>
								<span class="value">1116 15th St, Sacramento, USA</span>
							</div>
							<div class="c-item">
								<span class="icon"><i class="fa fa-phone"></i></span>
								<span class="value">0800 123 4567 <br/> 0800 123 4566</span>
							</div>
							<div class="c-item">
								<span class="icon"><i class="fa fa-envelope"></i></span>
								<span class="value"><a href="">energy@support.com</a> <br/> <a href="">energy@info.com</a></span>
							</div>
						</div>
					</div>
				</div>				
				<div class="col-md-6 col-lg-3">
					<div class="footer-block">
						<h3 class="block-title title-three">TWITTER</h3>
						<div class="block-desc">
							<div class="twitter-widget">
								<div class="feed">LinkedIn Workforce Report: January and February were the strongest consecutive months for hiring since August and September 2015</div>
								<div class="date"><span class="icon"><i class="fa fa-twitter"></i></span>2017/03/08 12:11:25</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3">
					<div class="footer-block">
						<h3 class="block-title title-three">Gallery</h3>
						<div class="block-desc">
							<div class="gallery">
								<div class="image-box">
									<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/carousel.jpg" /></div>
									<div class="overlay-one">
										<a href="#"><i class="fa fa-arrow"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="container">
			<?php printf( __( 'All copyrights reserved by %s', 'twentysixteen' ), 'WordPress' ); ?>
		</div>
	</div>
</footer>
<div class="overlay close-all"></div>
<?php wp_footer(); ?>
</body>
</html>