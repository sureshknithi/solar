<?php
/**
 * create custom theme options
 */

class Theme_Customize 
{
	public static $sections = array(
		'social' => array(
			'title' => 'Social',
			'priority' => 100,
			'capability' => 'edit_theme_options',
			'description' => 'Allows you to customize Social options'
		)
	);
	
	public static $controls = array(
		'facebook_control' => array(
			'label' => 'Facebbok URL',
            'section' => 'social',
            'settings' => 'facebook',
            'priority' => 10,
			'type' => 'text'
		),
		'twitter_control' => array(
			'label' => 'Twitter URL',
            'section' => 'social',
            'settings' => 'twitter',
            'priority' => 10,
			'type' => 'text'
		),
		'linkedin_control' => array(
			'label' => 'LinkedIn URL',
            'section' => 'social',
            'settings' => 'linkedin',
            'priority' => 10,
			'type' => 'text'
		),
		'pinterest_control' => array(
			'label' => 'Pinterest URL',
            'section' => 'social',
            'settings' => 'pinterest',
            'priority' => 10,
			'type' => 'text'
		),
		'youtube_control' => array(
			'label' => 'Youtube URL',
            'section' => 'social',
            'settings' => 'youtube',
            'priority' => 10,
			'type' => 'text'
		)
	);
	
	public static $settings = array(
		'facebook' => array(
			'default' => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'postMessage'
		),
		'twitter' => array(
			'default' => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'postMessage'
		),
		'linkedin' => array(
			'default' => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'postMessage'
		),
		'pinterest' => array(
			'default' => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'postMessage'
		),
		'youtube' => array(
			'default' => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'postMessage'
		)
	);
	
	public static function register ($wp_customize)
	{
		foreach(self::$sections as $name => $section) {
			$wp_customize->add_section($name, $section);
		}
		
		foreach(self::$settings as $name => $setting) {
			$wp_customize->add_setting($name, $setting);
		}

		foreach(self::$controls as $name => $control) {
			switch($control['type']){
				case 'color':
					$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, $name, $control));
					break;
				case 'image':
					$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, $name, $control));
					break;
				default:
					$wp_customize->add_control(new WP_Customize_Control($wp_customize, $name, $control));
					break;
			}
		}
	}
	
	public static function live_preview() {
		wp_enqueue_script('mytheme-themecustomizer', get_template_directory_uri().'/assets/js/theme-customizer.js', array(  'jquery', 'customize-preview' ), '', true);
	}
}

add_action( 'customize_register' , array( 'Theme_Customize' , 'register' ) );
add_action( 'customize_preview_init' , array( 'Theme_Customize' , 'live_preview' ) );