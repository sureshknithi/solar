<?php
$sliderId = $template_args['slider_id'];
$slider = get_page_by_path($sliderId, OBJECT, 'slider');
$images = acf_photo_gallery('images', $slider->ID);
?>
<div class="slider owl-carousel">
	<?php foreach($images as $image): ?>
	<div class="slide item">
		<img src="<?php echo $image['full_image_url']; ?>" />
		<div class="slide-overlay d-flex align-items-center justify-content-center">
			<div class="content-wrapper">
				<h2 class="slide-title"><?php echo $image['title']; ?></h2>
				<div class="slide-desc"><?php echo $image['caption']; ?></div>
				<?php if($image['url']): ?>
				<div class="slide-buttons">
					<a href="<?php echo $image['url']; ?>" <?php if($image['target']) echo 'target="_blank"'; ?> class="btn btn-primary">read more</a>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>