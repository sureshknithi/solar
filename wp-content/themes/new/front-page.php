<?php get_header(); ?>
<section class="what-we-offer text-center">
	<div class="container">
		<div class="title">
			<h2 class="title-one">What we Offer</h2>
			<span class="tagline-one">We offer different services</span>
		</div>
		<div class="desc-one">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent dapibus erat in tincidunt consequat. Phasellus sit amet convallis turpis, molestie nunc. Nulla facilisi. Vestibulum aliquam convallis ligula.
		</div>
		<div class="three-box">
			<div class="row">
				<?php for($i=1; $i<=3; $i++){ ?>
				<div class="col-lg-4">
					<div class="card image-card">
						<div class="image">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/card-bg.jpg" />
						</div>
						<div class="card-content">
							<div class="title"><a href="" class="title-two">Home appliances</a></div>
							<div class="desc">Maecenas maximus feugiat massa sed pharetra. Suspendisse ut sem ut justo condimentum congue at nec ex. Praesent </div>
							<div class="buttons">
								<a href="" class="btn btn-primary">view more</a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

<section class="our-works">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<span class="tagline-one">WE DO AWESOME WORKS</span>
				<h2 class="title-one">SOME OF OUR PROJECT HERE.</h2>
				<div class="desc-one">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent dapibus erat in tincidunt consequat. Phasellus sit amet convallis turpis, molestie nunc. Nulla facilisi. Vestibulum aliquam convallis ligula.
				</div>
				<div class="buttons">
					<a href="" class="btn btn-primary">see all projects</a>
				</div>
				<div class="slider owl-carousel" data-options='{"items": 2, "dots": false}'>
					<div class="image-box">
						<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/carousel.jpg" /></div>
						<div class="image-overlay d-flex align-items-center justify-content-center">
							<a href="#"><i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
					<div class="image-box">
						<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/carousel.jpg" /></div>
						<div class="image-overlay d-flex align-items-center justify-content-center">
							<a href="#"><i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="image-box">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/service.jpg" /></div>
					<div class="image-overlay d-flex align-items-center justify-content-center">
						<a href="#"><i class="fa fa-long-arrow-right"></i></a>
					</div>
				</div>
			</div>			
		</div>
	</div>
</section>

<section class="why-choose-us">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="title">
					<h2 class="title-one">WHY CHOOSE US</h2>
					<span class="tagline-one">We offer different services</span>
				</div>
				<div class="desc-one">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent dapibus erat in tincidunt consequat. Phasellus sit amet convallis turpis, id molestie nunc.
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-item">
					<div class="row">
						<div class="col-3">
							<div class="icon"><i class="fa fa-globe"></i></div>
						</div>
						<div class="col-9">
							<h2 class="title-three">WHY CHOOSE US</h2>
							<div class="desc-one">Fusce efficitur, tortor ac commodo preti, tellus dui gravida ante memori </div>
						</div>
					</div>
				</div>
				<div class="feature-item">
					<div class="row">
						<div class="col-3">
							<div class="icon"><i class="fa fa-globe"></i></div>
						</div>
						<div class="col-9">
							<h2 class="title-three">WHY CHOOSE US</h2>
							<div class="desc-one">Fusce efficitur, tortor ac commodo preti, tellus dui gravida ante memori </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-item">
					<div class="row">
						<div class="col-3">
							<div class="icon"><i class="fa fa-globe"></i></div>
						</div>
						<div class="col-9">
							<h2 class="title-three">WHY CHOOSE US</h2>
							<div class="desc-one">Fusce efficitur, tortor ac commodo preti, tellus dui gravida ante memori </div>
						</div>
					</div>
				</div>
				<div class="feature-item">
					<div class="row">
						<div class="col-3">
							<div class="icon"><i class="fa fa-globe"></i></div>
						</div>
						<div class="col-9">
							<h2 class="title-three">WHY CHOOSE US</h2>
							<div class="desc-one">Fusce efficitur, tortor ac commodo preti, tellus dui gravida ante memori </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="stats">
	<div class="container">
		<div class="stat-item">
			<div class="count">2477</div>
			<div class="label">FINISHED PROJECTS</div>
		</div>
		<div class="stat-item">
			<div class="count">2477</div>
			<div class="label">FINISHED PROJECTS</div>
		</div>
		<div class="stat-item">
			<div class="count">2477</div>
			<div class="label">FINISHED PROJECTS</div>
		</div>
		<div class="stat-item">
			<div class="count">2477</div>
			<div class="label">FINISHED PROJECTS</div>
		</div>
		<div class="stat-item">
			<div class="count">2477</div>
			<div class="label">FINISHED PROJECTS</div>
		</div>
	</div>
</section>

<section class="services parallax" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/service-bg.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-lg-6"></div>
			<div class="col-lg-6">
				<div class="title">
					<span class="tagline-one">FROM IDEA TO REALIZATION</span>
					<h2 class="title-one">SERVICES WE PROVIDE</h2>					
				</div>
				<div class="desc-one">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae arcu nec metus ultrices scelerisque facilisis id leo. Praesent iaculis lorem ut dictum tempus. Pellentesque sit amet erat at orci bibendum consequat.
				</div>
				<div class="service-list">
					<div class="row">
						<div class="col-lg-4">
							<div class="service-item">
								<div class="icon"><i class="fa fa-globe"></i></div>
								<a href="#" class="name">DEVELOPMENT IDEAS</a>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="service-item">
								<div class="icon"><i class="fa fa-globe"></i></div>
								<a href="#" class="name">DEVELOPMENT IDEAS</a>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="service-item">
								<div class="icon"><i class="fa fa-globe"></i></div>
								<a href="#" class="name">DEVELOPMENT IDEAS</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="service-item">
								<div class="icon"><i class="fa fa-globe"></i></div>
								<a href="#" class="name">DEVELOPMENT IDEAS</a>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="service-item">
								<div class="icon"><i class="fa fa-globe"></i></div>
								<a href="#" class="name">DEVELOPMENT IDEAS</a>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="service-item">
								<div class="icon"><i class="fa fa-globe"></i></div>
								<a href="#" class="name">DEVELOPMENT IDEAS</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog">
	<div class="container">
		<div class="title text-center">
			<h2 class="title-one">NEW FROM BLOG</h2>
			<span class="tagline-one">RECENT NEWS</span>
		</div>
		<div class="desc-one text-center">
			Curabitur dolor metus, accumsan vel iaculis eu, venenatis a turpis. Vestibulum mollis, nulla at tristique varius, ipsum diam tempus erat, nec dignissim ex lacus at velit. Etiam odio tortor, ultrices ultrices enim quis.
		</div>
		<div class="blog-list">
			<div class="row">
				<div class="col-lg-6">
					<div class="blog-image">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/blog.jpg">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="accordin-box">
						<?php for($i=1; $i<=4; $i++){?>
						<div class="post-item accordin-item">
							<div class="title-container">
								<div class="date d-flex align-items-center justify-content-center">14 Nov</div>
								<div class="post-title">
									<a class="title-three accordin-handle" href="javascript:;">FLANK JERKY COW SIRLOIN STRIP STEAK T-BONE BACON</a>
									<div class="clearfix">
										<a href="javascript:;" class="tagline-one pull-left category-link">Free Engergy</a>
										<span class="tagline-one comments pull-right"><i class="fa fa-comment"></i>22 comments</span>
									</div>
								</div>
							</div>
							<div class="accordin-content post-content">
								Doner tail strip steak venison beef ribs meatloaf leberkas ground round swine filet mignon alcatra short loin pork chop. Short loin t-bone tongue bresaola salami short ribs picanha spare ribs beef shoulder chuck shank ribeye. Boudin 
								<a href="#" class="view-more-btn">view more</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonial parallax text-center" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-bg.jpg')">
	<div class="container">
		<div class="title">
			<span class="tagline-one">WHAT PEOPLE SAY</span>
			<h2 class="title-one">OUR TESTIMONIALS</h2>					
		</div>
		<div class="testimonial-list owl-carousel" data-options='{"items": 3, "nav": false}'>
			<?php for($i=1; $i<=3; $i++){?>
			<div class="testimonial-item">
				<div class="image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/author.jpg"></div>
				<div class="desc-two">
					Duis congue purus non malesuada porttitor. Integer vitae orci ac orci tincidunt efficitur. Sed tristique magna a nulla porttitor congue.
				</div>
				<div class="title-three">DERRICK RODGERS</div>
				<div class="position">Manager</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<section class="contact-us">
	<div class="container">
		<div class="text-center">
			<div class="title">
				<span class="tagline-one">CONNECT WITH US</span>
				<h2 class="title-one">OUR CONTACTS</h2>				
			</div>
			<div class="desc-one">
				Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce tempus odio vitae nibh semper, ac venenatis massa mollis. Proin tempus, mauris eu facilisis feugiat, nisi augue faucibus.
			</div>
		</div>
		<div class="contact-data">
			<div class="row">
				<div class="col-md-4">
					<div class="contact-item">
						<div class="icon"><i class="fa fa-map-marker"></i></div>
						<div class="detail">
							<div class="title-three">HEAD OFFICE</div>
							<div class="value">1116 15th St, Sacramento, <br/> California, CA 95814, USA</div>
						</div>
					</div>					
				</div>
				<div class="col-md-4">
					<div class="contact-item">
						<div class="icon"><i class="fa fa-map-marker"></i></div>
						<div class="detail">
							<div class="title-three">HEAD OFFICE</div>
							<div class="value">1116 15th St, Sacramento, <br/> California, CA 95814, USA</div>
						</div>
					</div>					
				</div>
				<div class="col-md-4">
					<div class="contact-item">
						<div class="icon"><i class="fa fa-envelope"></i></div>
						<div class="detail">
							<div class="title-three">HEAD OFFICE</div>
							<div class="value"><a href="">energy@support.com</a> <br/> <a href="">energy@info.com</a></div>
						</div>
					</div>					
				</div>
			</div>
		</div>
		<div class="contact-form">
			<div class="title text-center">
				<span class="tagline-one">Say Hey!</span>
				<h2 class="title-one">CONTACT FORM</h2>
			</div>
			<div class="form-container">
				<form method="post">
					<div class="row">
						<div class="col-sm-6">
							<input type="text" placeholder="Enter your name">
						</div>
						<div class="col-sm-6">
							<input type="text" placeholder="Enter your email">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<input type="text" placeholder="Enter your subject">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<textarea placeholder="Enter your message"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="buttons text-center">
								<button type="submit" class="btn btn-primary">send message</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<section class="brands">
	<div class="container-fluid">
		<div class="row">
			<?php for($i=1; $i<=6; $i++) { ?>
			<div class="col-lg-2">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/brand.png" />
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>