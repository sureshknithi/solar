//Global Variables
var Global = {
	winWidth: window.innerWidth,
	winHeight: window.innerHeight,
	small: 576,
	medium: 768,
	large: 992,
	extraLarge: 1200,
	screen: null
};

jQuery(document).ready(function(){
	Theme.setupScreen();
	Theme.setupHeader();
	Theme.setupSlider();
	Accordin.init();
	jQuery('.toggle-menu').click(Menu.toggle);
	jQuery('.close-all').click(function(){
		Menu.close();
	});
});

var Theme = {
	setupScreen: function() {
		switch(true) {
			case (Global.winWidth >= Global.extraLarge):
				Global.screen = 'xl';
				break;
			case (Global.winWidth >= Global.large):
				Global.screen = 'lg';
				break;
			case (Global.winWidth >= Global.medium):
				Global.screen = 'md';
				break;
			case (Global.winWidth >= Global.small):
				Global.screen = 'sm';
				break;
			default:
				Global.screen = 'xs';
				break;
		}
	},
	setupHeader: function() {		
		jQuery(window).scroll(function(){
			var top = jQuery(window).scrollTop();
			if(top > 0) {
				jQuery('header').addClass('fixed');
			} else {
				jQuery('header').removeClass('fixed');
			}
		});
	},
	setupSlider: function() {
		jQuery('.owl-carousel').each(function(){
			var defaultOptions = {items: 1, nav:true, loop: true, dots: true, navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>']};
			var options = jQuery(this).data('options');
			var owlOptions = jQuery.extend( defaultOptions, options );
			jQuery(this).owlCarousel(owlOptions);
		});
	}
};

var Menu = {
	isOpen: null,
	menuId: '.header-menu',
	open: function() {
		jQuery(Menu.menuId+', .overlay').addClass('active');
		Menu.isOpen = true;
	},
	close: function() {
		jQuery(Menu.menuId+', .overlay').removeClass('active');
		Menu.isOpen = false;
	},
	toggle: function() {
		jQuery(Menu.menuId+', .overlay').toggleClass('active');
		Menu.isOpen = !Menu.isOpen;
	},
	bindEvents: function() {
		
	}
};

var Accordin = {
	init: function() {
		jQuery('.accordin-item .accordin-handle').click(function(){
			if(jQuery(this).parents('.accordin-item').hasClass('active')) {
				jQuery('.accordin-item .accordin-content').slideUp();
				jQuery(this).parents('.accordin-item').removeClass('active');
			} else {
				jQuery('.accordin-item .accordin-content').slideUp();
				jQuery(this).parents('.accordin-item').find('.accordin-content').slideDown();
				jQuery('.accordin-item').removeClass('active');
				jQuery(this).parents('.accordin-item').addClass('active');
			}		
		});
		jQuery('.accordin-item:first-child .accordin-handle').trigger('click');
	}
};

