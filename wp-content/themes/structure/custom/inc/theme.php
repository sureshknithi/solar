<?php
/*
 * Custom functions
 */
 
function hide_admin_bar_from_front_end(){
	//if (is_blog_admin()) { return true; }
	return false;
}
add_filter( 'show_admin_bar', 'hide_admin_bar_from_front_end' );

function customtheme_scripts() {
	wp_enqueue_style( 'theme-custom', THEME_ROOT . '/custom/css/theme.css' );
}
add_action( 'wp_enqueue_scripts', 'customtheme_scripts' );